@extends('layouts.main')

@section('title', "La tâche")

@section ('content')

<h2>{{$task->title}}</h2>
<p>{{$task->description}}</p>
<p>Due Date : {{$task->due_date}}</p>
<p>State : {{$task->state}}</p>
<br/>
<p>Créer le : {{$task->created_at}}</p>
<p> Modifié le : {{$task->updated_at}}</p>
<br/>
<br/>

<h3>Participants assignés à la tâche</h3>

    @foreach($task->assignedUsers as $user)

    {{$user->id}}
    {{$user->name}}
 <form action="{{route('taskUser.destroy',$user->pivot)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit"> Supprimer </button> 
    </form>
    
    
    @endforeach

<a class="btn btn-primary" href="{{route('tasks.taskUser.create',$task->id)}}"> Ajouter un participant</a>

<br/><br/><br/>

<h3>Commentaires</h3>
    @foreach($task->comments as $comment)

    {{$comment->user->name}}
{{$comment->text}}
{{$comment->updated_at}}
 <form action="" method="POST">
        @csrf
        @method('PUT')
        <input value={{$comment->text}} type="text" id="text" name="text" class=@error('text')  is-invalid @enderror>
        @error('text')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button class="btn btn-success" type="submit">Modifier</button>
    </form>
 <form action="" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit"> Supprimer </button> 
    </form>
    
    
    @endforeach

<form action="" method="POST">
    @csrf
    <label for='text'> Commentaire </label>
    <input type="text" id="text" name="text" class=@error('text') is-invalid @enderror>
    @error('text')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button class="btn btn-primary" type="submit">Ajouter</button>
</form>

<br/><br/><br/>

<h3>Documents</h3>
    @foreach($task->attachments as $attachment)

    {{$attachment->user->name}}
    {{$attachment->text}}
    {{$attachment->updated_at}}
 <form action="" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit"> Supprimer </button> 
    </form>
    
    
    @endforeach

<form action="" method="POST">
    @csrf
    <label for='file'> Documents </label>
    <input type="file" name="file" class=@error('file') is-invalid @enderror>
    @error('file')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button class="btn btn-primary" type="submit">Ajouter</button>
</form>