@extends('layouts.main')

@section('title', "Les boards {{$board->title}}")

@section ('content')

<h2>{{$board->title}}</h2>
<p>{{$board->description}}</p>
<br/>
<br/>
<h3>Participants :</h3>
    @foreach($board->users as $user)
{{$user->name}}
 <form action="{{route('boardUser.destroy',$user->pivot)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit"> Supprimer </button> 
    </form>
    
    
    @endforeach

</table>
<a class="btn btn-primary" href="{{route('boards.boardUser.create',$board->id)}}"> Ajouter un participant</a>
<br/>
<br/>
<br/>

<h3>Tâches :</h3>

    @foreach($board->tasks as $task)
    {{$task->title}}
    {{$task->description}}
    {{$task->due_date}}
    {{$task->id}}
     <a class="btn btn-info" href="{{route('tasks.show',$task->id)}}"> Voir</a>
     <a class="btn btn-success" href="{{route('tasks.edit',$task->id)}}"> Modifer</a>
     <form action="{{route('tasks.destroy',$task->id)}}" method="post">
           @csrf
           @method('DELETE')
           <button class="btn btn-danger" type="submit"> Supprimer </button> 
       
    
    </form>
       @endforeach
<a class="btn btn-primary" href="{{route('boards.tasks.create',$board->id)}}"> Ajouter une tâche</a>