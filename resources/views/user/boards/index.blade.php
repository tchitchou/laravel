@extends('layouts.main')

@section('title', "Les boards de l'utilisateur")

@section ('content')

@foreach ($boards as $board)

    {{$board->title}}
    {{$board->description}}
     <a class="btn btn-info" href="{{route('boards.show',$board->id)}}"> Voir</a>
     <a class="btn btn-success" href="{{route('boards.edit',$board->id)}}"> Modifer</a>
     <form action="{{route('boards.destroy',$board->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit"> Supprimer </button> 
        </form>
        
    
@endforeach
</table>

<a class="btn btn-primary" href="{{route('boards.create')}}"> Ajouter un board</a>

@endsection