<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
    <head>
        <title>App Name - @yield('title')</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- Styles -->
        <style>
           .is-invalid {border:1px solid red}
        
        </style>
    </head>
    <body>
        
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>