Réalisé :

- ajouter un board avec titre et description

- supprimer/voir/modifier board

    * modifier 
        -> nom et description

    * voir 
	    -> ajouter/supprimer participant
	    -> ajouter/supprimer/voir/modifier tâche

    * ajouter tâche

        -> titre, description, due_date, catégorie, state

    * modifier tâche

    * voir tâche 
        -> affiche tout ce que nous avons rentré et l'heure à laquelle la tâche a été ajoutée et modifiée
        -> Nous avons voulu mettre en place l'ajour de commentaire, mais n'est pas terminé
        -> On peut ajouter des pièces jointes

Ce projet était assez complexe. Sans un travail d'entraide et de soutien cela aurait été presque infaisable pour ma part. 
Comme vous pourrez le constater, le travail n'est pas totalement achevé, ceci étant dû à un manque de motivation du fait d'avoir passé beaucoup de temps dessus.
Les difficultés étant tellement nombreuses, je ne peux les lister, le moindre ajout ou changement dans le code faisait planter le projet.